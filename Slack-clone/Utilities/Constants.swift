//
//  Constants.swift
//  Slack-clone
//
//  Created by Dhruv Vaghela on 23/12/17.
//  Copyright © 2017 Dhruv Vaghela. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ Success: Bool) -> ()

// Segues
let TO_LOGIN = "toLogin"
let TO_CREATEACCOUNT = "toCreateAccount"
let UNWIND = "unwindToChannel"

// User defaults
let TOKEN_KEY = "token"
let LOGGED_IN_KEY = "loggedIn"
let USER_EMAIL = "userEmail"

// URL constants
let BASE_URL = "https://slack-clone-dv.herokuapp.com/"
let URL_REGISTER = ""
