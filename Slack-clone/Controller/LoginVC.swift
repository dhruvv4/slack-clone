//
//  LoginVC.swift
//  Slack-clone
//
//  Created by Dhruv Vaghela on 23/12/17.
//  Copyright © 2017 Dhruv Vaghela. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func closePressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil) 
    }
    @IBAction func createAccountBtnPressed(_ sender: UIButton) {
        performSegue(withIdentifier: TO_CREATEACCOUNT, sender: nil)
    }
}
