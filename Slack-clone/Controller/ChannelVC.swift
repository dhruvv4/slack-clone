//
//  ChannelVC.swift
//  Slack-clone
//
//  Created by Dhruv Vaghela on 22/12/17.
//  Copyright © 2017 Dhruv Vaghela. All rights reserved.
//

import UIKit

class ChannelVC: UIViewController {

    // Outlets
    @IBOutlet weak var loginBtn: UIButton!
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue){}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Increase the width of the channel menu view.
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
    }
    
    
    
    @IBAction func loginBtnPressed(_ sender: UIButton) {
        performSegue(withIdentifier: TO_LOGIN, sender: nil)
    }
    
    

}
