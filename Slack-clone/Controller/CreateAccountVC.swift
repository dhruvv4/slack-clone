//
//  CreateAccountVC.swift
//  Slack-clone
//
//  Created by Dhruv Vaghela on 23/12/17.
//  Copyright © 2017 Dhruv Vaghela. All rights reserved.
//

import UIKit

class CreateAccountVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func closeBtnPressed(_ sender: UIButton) {
        performSegue(withIdentifier: UNWIND, sender: nil)
    }
}
