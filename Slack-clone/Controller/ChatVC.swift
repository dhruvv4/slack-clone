//
//  ChatVC.swift
//  Slack-clone
//
//  Created by Dhruv Vaghela on 22/12/17.
//  Copyright © 2017 Dhruv Vaghela. All rights reserved.
//

import UIKit

class ChatVC: UIViewController {

    // Outlets
    
    @IBOutlet weak var menuBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Menu button action to open the channel view from the chat view.
        menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
        
        // Pan gesture to slide the rear views.
        // Eg: To open/close the channel-menu with a pan/semi swipe.
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        // Touch gesture to close the rear views.
        // Eg: To close the channel-menu with a tap.
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
    }

}
